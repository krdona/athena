/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
 */

#include "AthenaPoolExampleData/versions/ExampleElectronAuxContainer_v1.h"

namespace xAOD {

ExampleElectronAuxContainer_v1::ExampleElectronAuxContainer_v1()
    : xAOD::AuxContainerBase() {}

}  // namespace xAOD
