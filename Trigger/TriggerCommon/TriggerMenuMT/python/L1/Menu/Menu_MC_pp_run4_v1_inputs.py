# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

import TriggerMenuMT.L1.Menu.Menu_MC_pp_run3_v1_inputs as Run3

def defineInputsMenu():
    # reuse based on run3 menu
    Run3.defineInputsMenu()
